/**
 *
 */
package fr.ge.record.ws.v1.model;

/**
 * Enumeration of all type actions possible for a record.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public enum RecordActionTypeEnum {

    /** Action : read the record. */
    READ("read"),

    /** Action : tracker history. */
    HISTORY("history"),

    /** Action : delete the record. */
    DELETE("delete"),

    /** Action : download the record or the cerfa generated from the record. */
    DOWNLOAD("download"),

    /**
     * Action : download the generated documents specified un meta "download"
     */
    DOCUMENTS("documents");

    /** the name of the type action, used for example for the CSS class. */
    private String typeActionName;

    /**
     * Constructor.
     *
     * @param typeActionName
     *            type action name
     */
    RecordActionTypeEnum(final String typeActionName) {
        this.typeActionName = typeActionName;
    }

    /**
     * Accessor on {@link #typeActionName}.
     *
     * @return String typeActionName
     */
    public String getTypeActionName() {
        return this.typeActionName;
    }

}
