/**
 * 
 */
package fr.ge.record.ws.v1.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "label")
@XmlAccessorType(XmlAccessType.FIELD)
public class LocalizedLabelBO {

  /** Default language. */
  private String defaultLocale;

  /** Set of available labels. */
  private List < LocalizedStringBO > values;

  /**
   * The label chosen according to the default languages of the daughter application and the default
   * languages of the dashboard.
   */
  private String actualLabel;

  /**
   * Accesseur sur l'attribut {@link #defaultLocale}.
   *
   * @return String defaultLocale
   */
  public String getDefaultLocale() {
    return defaultLocale;
  }

  /**
   * Mutateur sur l'attribut {@link #defaultLocale}.
   *
   * @param defaultLocale
   *          la nouvelle valeur de l'attribut defaultLocale
   */
  public void setDefaultLocale(final String defaultLocale) {
    this.defaultLocale = defaultLocale;
  }

  /**
   * Accesseur sur l'attribut {@link #values}.
   *
   * @return List<RecordDashboardLocalizedString> values
   */
  public List < LocalizedStringBO > getValues() {
    return values;
  }

  /**
   * Mutateur sur l'attribut {@link #values}.
   *
   * @param values
   *          la nouvelle valeur de l'attribut values
   */
  public void setValues(final List < LocalizedStringBO > values) {
    this.values = values;
  }

  /**
   * Accesseur sur l'attribut {@link #actualLabel}.
   *
   * @return String actualLabel
   */
  public String getActualLabel() {
    if (org.apache.commons.lang3.StringUtils.isBlank(this.actualLabel)) {
      for (LocalizedStringBO translation : this.values) {
        if (translation.getLocale().equals(this.defaultLocale)) {
          return translation.getValue();
        }
      }
    }
    return this.actualLabel;
  }

  /**
   * Mutateur sur l'attribut {@link #actualLabel}.
   *
   * @param actualLabel
   *          la nouvelle valeur de l'attribut actualLabel
   */
  public void setActualLabel(final String actualLabel) {
    this.actualLabel = actualLabel;
  }
}
