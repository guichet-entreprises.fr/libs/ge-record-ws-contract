/**
 * 
 */
package fr.ge.record.ws.v1.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.record.ws.v1.model.RecordResultBO;

/**
 * Service interface to handle records.
 * 
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
public interface IRecordBOService {

    /**
     * Search Records For DASHBOARD BACK-OFFICE.
     * 
     * @param startIndex
     *            the start index
     * @param maxResults
     *            the number of record per page
     * @param orders
     *            the sort criteria
     * @param fullTextSearchCriteria
     *            the text to search
     * @param engineUserType
     *            the engine user type
     * @return a list of BACK-OFFICE records
     */
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Path("/record/v2/search")
    RecordResultBO searchRecordsBO(@QueryParam("startIndex") int startIndex, @QueryParam("maxResults") int maxResults, @QueryParam("orders") String orders,
            @QueryParam("searchText") String fullTextSearchCriteria, @QueryParam("beginDate") String beginDate, @QueryParam("endDate") String endDate, @QueryParam("archived") Boolean archived,
            @QueryParam("engineUserType") String engineUserType);

}
