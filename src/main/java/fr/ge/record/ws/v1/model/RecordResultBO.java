/**
 *
 */
package fr.ge.record.ws.v1.model;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "results")
@XmlAccessorType(XmlAccessType.FIELD)
public class RecordResultBO extends AbstractDashboardResult<RecordBODisplay> {

    private RecordDataSource dataSource;

    private int totalPages;

    private int maxPerPage = 12;

    private int page = 1;

    public RecordResultBO() {

    }

    /**
     * Accesseur sur l'attribut {@link #dataSource}.
     *
     * @return RecordDataSource dataSource
     */
    public RecordDataSource getDataSource() {
        return dataSource;
    }

    /**
     * Mutateur sur l'attribut {@link #dataSource}.
     *
     * @param dataSource
     *            la nouvelle valeur de l'attribut dataSource
     */
    public RecordResultBO setDataSource(final RecordDataSource dataSource) {
        this.dataSource = dataSource;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #totalPages}.
     *
     * @return int totalPages
     */
    public int getTotalPages() {
        return Optional.ofNullable(this.totalPages).orElse(1);
    }

    /**
     * Mutateur sur l'attribut {@link #totalPages}.
     *
     * @param totalPages
     *            la nouvelle valeur de l'attribut totalPages
     */
    public RecordResultBO setTotalPages(final int totalPages) {
        this.totalPages = totalPages;
        return this;
    }

    public List<Integer> pageNumbers() {
        return IntStream.rangeClosed(1, this.getTotalPages()).boxed().collect(Collectors.toList());
    }

    /**
     * Accesseur sur l'attribut {@link #page}.
     *
     * @return int page
     */
    public int getPage() {
        return page;
    }

    /**
     * Mutateur sur l'attribut {@link #page}.
     *
     * @param page
     *            la nouvelle valeur de l'attribut page
     */
    public RecordResultBO setPage(final int page) {
        this.page = page;
        return this;
    }

    public int rangeStart() {
        return 1;
    }

    public int prevPage() {
        return Math.max(this.rangeStart(), this.page - 1);
    }

    public int nextPage() {
        return Math.min(this.page + 1, this.getTotalPages());
    }

    public boolean showPage(int page) {
        if (this.getTotalPages() > 8) {
            if ((page - this.page < 4 && page - this.page >= 0 && this.page < 4) || page - this.page < 3 && this.page <= page || page == 1
                    || (this.getTotalPages() - this.page < 4 && this.getTotalPages() - page < 4 && this.page > 4) || this.getTotalPages() - page == 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}
