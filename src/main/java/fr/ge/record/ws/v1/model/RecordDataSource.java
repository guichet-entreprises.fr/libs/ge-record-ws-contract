/**
 * 
 */
package fr.ge.record.ws.v1.model;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class RecordDataSource {
    private String key;
    private String engineUserType;
    private String label;
    private String publicUrl;
    private String privateUrl;

    /**
     * Accesseur sur l'attribut {@link #key}.
     *
     * @return String key
     */
    public String getKey() {
        return key;
    }

    /**
     * Mutateur sur l'attribut {@link #key}.
     *
     * @param key
     *            la nouvelle valeur de l'attribut key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Accesseur sur l'attribut {@link #label}.
     *
     * @return String label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Mutateur sur l'attribut {@link #label}.
     *
     * @param label
     *            la nouvelle valeur de l'attribut label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Accesseur sur l'attribut {@link #publicUrl}.
     *
     * @return String publicUrl
     */
    public String getPublicUrl() {
        return publicUrl;
    }

    /**
     * Mutateur sur l'attribut {@link #publicUrl}.
     *
     * @param publicUrl
     *            la nouvelle valeur de l'attribut publicUrl
     */
    public void setPublicUrl(final String publicUrl) {
        this.publicUrl = publicUrl;
    }

    /**
     * Accesseur sur l'attribut {@link #privateUrl}.
     *
     * @return String privateUrl
     */
    public String getPrivateUrl() {
        return privateUrl;
    }

    /**
     * Mutateur sur l'attribut {@link #privateUrl}.
     *
     * @param privateUrl
     *            la nouvelle valeur de l'attribut privateUrl
     */
    public void setPrivateUrl(final String privateUrl) {
        this.privateUrl = privateUrl;
    }

    @Override
    public String toString() {
        return String.format("'%s' with public url : [%s], private url : [%s] and engine user type : [%s]", this.getLabel(), this.getPublicUrl(), this.getPrivateUrl(), this.getEngineUserType());
    }

    /**
     * Accesseur sur l'attribut {@link #engineUserType}.
     *
     * @return String engineUserType
     */
    public String getEngineUserType() {
        return engineUserType;
    }

    /**
     * Mutateur sur l'attribut {@link #engineUserType}.
     *
     * @param engineUserType
     *            la nouvelle valeur de l'attribut engineUserType
     */
    public void setEngineUserType(final String engineUserType) {
        this.engineUserType = engineUserType;
    }
}
