/**
 *
 */
package fr.ge.record.ws.v1.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
public enum RecordActionTypeBOEnum {

    /** Action : delete the record. */
    DELETE("confirm:delete", "delete"),

    /** Action : export the record. */
    EXPORT("confirm:export", "export"),

    /** Action : view summary of the record. */
    VIEW("view", "summary"),

    /** Action : download the record or the cerfa generated from the record. */
    DOWNLOAD("download", "download"),

    /**
     * Action : download the generated documents specified un meta "download"
     */
    DOCUMENTS("documents", "download"),

    /** Action : Archive a record. */
    EXEC_ARCHIVED("exec:archived", "archived"),

    /** Action : Process a record. */
    EXEC_PROCESSED("exec:processed", "processed");

    /** the name of the type action, used for example for the CSS class. */
    private String typeActionName;

    /** Type Action ID. **/
    private String typeActionId;

    /** the constant EXECUTE_STEP_PATTERN. */
    private static final Pattern EXECUTE_STEP_PATTERN = Pattern.compile("^(exec:)(\\w+)");

    /** the constant CONFIRM_STEP_PATTERN. */
    private static final Pattern CONFIRM_STEP_PATTERN = Pattern.compile("^(confirm:)(\\w+)");

    /**
     * Constructeur de la classe.
     *
     * @param typeActionName
     * @param typeActionId
     */
    private RecordActionTypeBOEnum(final String typeActionName, final String typeActionId) {
        this.typeActionName = typeActionName;
        this.typeActionId = typeActionId;
    }

    /**
     * Get RecordActionTypeBOEnum By typeActionName.
     *
     * @param typeActionName
     *            type Action Name
     * @return
     */
    public static RecordActionTypeBOEnum get(final String typeActionName) {
        RecordActionTypeBOEnum result = null;
        for (final RecordActionTypeBOEnum item : RecordActionTypeBOEnum.values()) {
            if (item.getTypeActionName().equals(typeActionName)) {
                result = item;
            }
        }
        return result;
    }

    /**
     * Accesseur sur l'attribut {@link #typeActionId}.
     *
     * @return String typeActionId
     */
    public String getTypeActionId() {
        return this.typeActionId;
    }

    /**
     * Mutateur sur l'attribut {@link #typeActionId}.
     *
     * @param typeActionId
     *            la nouvelle valeur de l'attribut typeActionId
     */
    public void setTypeActionId(final String typeActionId) {
        this.typeActionId = typeActionId;
    }

    /**
     * Accessor on {@link #typeActionName}.
     *
     * @return String typeActionName
     */
    public String getTypeActionName() {
        return this.typeActionName;
    }

    /**
     * Return true if the input action is a step action.
     *
     * @param action
     *            The action identifier
     * @return boolean
     */
    public static boolean isExecAction(final String action) {
        for (final RecordActionTypeBOEnum item : RecordActionTypeBOEnum.values()) {
            final String typeActionName = item.getTypeActionName();
            if (typeActionName.equals(action) && matchesExecAction(action)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if the action is a record execute action.
     *
     * @param action
     *            The action identifier
     * @return true or false
     */
    public static boolean matchesExecAction(final String action) {
        return EXECUTE_STEP_PATTERN.matcher(action).matches();
    }

    /**
     * Return the step identifier for an execute action step.
     *
     * @param action
     *            The action identifier
     * @return The step id
     */
    public static String getExecStepId(final String action) {
        if (isExecAction(action)) {
            final Matcher matcherExec = EXECUTE_STEP_PATTERN.matcher(action);
            if (matcherExec.matches()) {
                return matcherExec.group(2);
            }
        }
        return action;
    }

    /**
     * @param action
     * @return
     */
    public static boolean isConfirmAction(String action) {
        for (final RecordActionTypeBOEnum item : RecordActionTypeBOEnum.values()) {
            final String typeActionName = item.getTypeActionName();
            if (typeActionName.equals(action) && CONFIRM_STEP_PATTERN.matcher(action).matches()) {
                return true;
            }
        }
        return false;
    }
}