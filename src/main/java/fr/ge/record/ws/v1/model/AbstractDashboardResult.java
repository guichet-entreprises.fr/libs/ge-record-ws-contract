/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.record.ws.v1.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Christian Cougourdan
 */
@XmlRootElement(name = "results")
@XmlAccessorType(XmlAccessType.FIELD)
public class AbstractDashboardResult<T> {

    /** The list of records returned in this page. */
    private List<T> records;

    /** Returned records list page's start index. */
    private String startIndex;

    /** Total elements available. */
    private String totalResults;

    /** Returned records list page's total elements. */
    private String nbResults;

    /**
     * Accesseur sur l'attribut {@link #records}.
     *
     * @return List<RecordDisplay> records
     */
    public List<T> getRecords() {
        return this.records;
    }

    /**
     * Mutateur sur l'attribut {@link #records}.
     *
     * @param records
     *            la nouvelle valeur de l'attribut records
     */
    public void setRecords(final List<T> records) {
        this.records = records;
    }

    /**
     * Accesseur sur l'attribut {@link #startIndex}.
     *
     * @return String startIndex
     */
    public String getStartIndex() {
        return this.startIndex;
    }

    /**
     * Mutateur sur l'attribut {@link #startIndex}.
     *
     * @param startIndex
     *            la nouvelle valeur de l'attribut startIndex
     */
    public void setStartIndex(final String startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * Accesseur sur l'attribut {@link #totalResults}.
     *
     * @return String totalResults
     */
    public String getTotalResults() {
        return this.totalResults;
    }

    /**
     * Mutateur sur l'attribut {@link #totalResults}.
     *
     * @param totalResults
     *            la nouvelle valeur de l'attribut totalResults
     */
    public void setTotalResults(final String totalResults) {
        this.totalResults = totalResults;
    }

    /**
     * Accesseur sur l'attribut {@link #nbResults}.
     *
     * @return String nbResults
     */
    public String getNbResults() {
        return this.nbResults;
    }

    /**
     * Mutateur sur l'attribut {@link #nbResults}.
     *
     * @param nbResults
     *            la nouvelle valeur de l'attribut nbResults
     */
    public void setNbResults(final String nbResults) {
        this.nbResults = nbResults;
    }

}
