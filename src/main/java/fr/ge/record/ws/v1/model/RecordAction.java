/**
 * 
 */
package fr.ge.record.ws.v1.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information of an action for a record shown in the dashboard.
 * 
 * @author $Author: mtakerra $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "action")
@XmlAccessorType(XmlAccessType.FIELD)
public class RecordAction implements Serializable {

  /** UID. */
  private static final long serialVersionUID = -2921792853318460726L;

  /** Type of the action. */
  private RecordActionTypeEnum type;

  /** Label of the action. */
  private LocalizedLabel label;

  /** URI of the action. */
  private String uri;

  /**
   * Accesseur sur l'attribut {@link #type}.
   *
   * @return RecordActionTypeEnum type
   */
  public RecordActionTypeEnum getType() {
    return this.type;
  }

  /**
   * Mutateur sur l'attribut {@link #type}.
   *
   * @param type
   *          la nouvelle valeur de l'attribut type
   */
  public void setType(final RecordActionTypeEnum type) {
    this.type = type;
  }

  /**
   * Accesseur sur l'attribut {@link #label}.
   *
   * @return LocalizedLabel label
   */
  public LocalizedLabel getLabel() {
    return label;
  }

  /**
   * Mutateur sur l'attribut {@link #label}.
   *
   * @param label
   *          la nouvelle valeur de l'attribut label
   */
  public void setLabel(final LocalizedLabel label) {
    this.label = label;
  }

  /**
   * Accesseur sur l'attribut {@link #uri}.
   *
   * @return String uri
   */
  public String getUri() {
    return uri;
  }

  /**
   * Mutateur sur l'attribut {@link #uri}.
   *
   * @param uri
   *          la nouvelle valeur de l'attribut uri
   */
  public void setUri(final String uri) {
    this.uri = uri;
  }
}
