/**
 * 
 */
package fr.ge.record.ws.v1.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.core.exception.FunctionalException;
import fr.ge.core.exception.TechniqueException;
import fr.ge.record.ws.v1.model.RecordResult;

/**
 * Rest Web Service interface to display records.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public interface IRecordDisplayRestService {

  /**
   * Search for records with text searching of an user, delimiting by a index and a max results. The
   * result can be ordered by create date, update date or in title alphabetical order.
   *
   * @param authorId
   *          id of the record's author
   * @param startIndex
   *          start index (>=0, by default = 0)
   * @param maxResults
   *          Number maximum of records returned. (>=1, by default = 1)
   * @param orders
   *          Possible values : creation, update, title. Default : update.
   * @param searchText
   *          text for searching records
   * @return {@link RecordResult}
   */
  @GET
  @Produces({MediaType.APPLICATION_JSON })
  @Path("/record/author/{authorId}/search")
  RecordResult searchRecords(@PathParam("authorId") String authorId, @QueryParam("startIndex") int startIndex,
    @QueryParam("maxResults") int maxResults, @QueryParam("orders") String orders, @QueryParam("searchText") String searchText)
    throws TechniqueException, FunctionalException;

  /**
   * Search for records of an user delimiting by a index and a max results. The result can be
   * ordered by create date, update date or in title alphabetical order.
   *
   * @param authorId
   *          id of the record's author
   * @param startIndex
   *          start index (>=0, by default = 0)
   * @param maxResults
   *          Number maximum of records returned. (>=1, by default = 1)
   * @param orders
   *          Possible values : creation, update, title. Default : update.
   * @return {@link RecordResult}
   */
  @GET
  @Produces({MediaType.APPLICATION_JSON })
  @Path("/record/author/{authorId}")
  RecordResult getRecords(@PathParam("authorId") String authorId, @QueryParam("startIndex") int startIndex,
    @QueryParam("maxResults") int maxResults, @QueryParam("orders") String orders);
}
