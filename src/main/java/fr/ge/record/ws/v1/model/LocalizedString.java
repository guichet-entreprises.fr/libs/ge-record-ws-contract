/**
 * 
 */
package fr.ge.record.ws.v1.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Localized String, meant to be used with {@link LocalizedLabel}.
 * 
 * @author $Author: mtakerra $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "values")
@XmlAccessorType(XmlAccessType.FIELD)
public class LocalizedString {

  /** Language. */
  private String locale;

  /** Label of the text in the locale language. */
  private String value;

  /**
   * Default constructor.
   */
  public LocalizedString() {
    // Nothing to do.
  }

  /**
   * Constructor.
   *
   * @param locale
   *          the locale
   * @param value
   *          the value
   */
  public LocalizedString(final String locale, final String value) {
    this.locale = locale;
    this.value = value;
  }

  /**
   * Accesseur sur l'attribut {@link #locale}.
   *
   * @return String locale
   */
  public String getLocale() {
    return locale;
  }

  /**
   * Mutateur sur l'attribut {@link #locale}.
   *
   * @param locale
   *          la nouvelle valeur de l'attribut locale
   */
  public void setLocale(final String locale) {
    this.locale = locale;
  }

  /**
   * Accesseur sur l'attribut {@link #value}.
   *
   * @return String value
   */
  public String getValue() {
    return value;
  }

  /**
   * Mutateur sur l'attribut {@link #value}.
   *
   * @param value
   *          la nouvelle valeur de l'attribut value
   */
  public void setValue(final String value) {
    this.value = value;
  }

}
