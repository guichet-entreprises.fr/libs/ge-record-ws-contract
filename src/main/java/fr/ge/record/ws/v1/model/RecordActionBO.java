/**
 * 
 */
package fr.ge.record.ws.v1.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "action")
@XmlAccessorType(XmlAccessType.FIELD)
public class RecordActionBO implements Serializable {

    /** UID. */
    private static final long serialVersionUID = -1454347267098630648L;

    /** Type of the action. */
    private String type;

    /** Label of the action. */
    private LocalizedLabelBO label;

    /** Action code. **/
    private String code;

    /**
     * Accesseur sur l'attribut {@link #type}.
     *
     * @return String type
     */
    public String getType() {
        return type;
    }

    /**
     * Mutateur sur l'attribut {@link #type}.
     *
     * @param type
     *            la nouvelle valeur de l'attribut type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Accesseur sur l'attribut {@link #label}.
     *
     * @return LocalizedLabel label
     */
    public LocalizedLabelBO getLabel() {
        return label;
    }

    /**
     * Mutateur sur l'attribut {@link #label}.
     *
     * @param label
     *            la nouvelle valeur de l'attribut label
     */
    public void setLabel(final LocalizedLabelBO label) {
        this.label = label;
    }

    /**
     * Accesseur sur l'attribut {@link #code}.
     *
     * @return String code
     */
    public String getCode() {
        return code;
    }

    /**
     * Mutateur sur l'attribut {@link #code}.
     *
     * @param code
     *            la nouvelle valeur de l'attribut code
     */
    public void setCode(String code) {
        this.code = code;
    }
}
