/**
 *
 */
package fr.ge.record.ws.v1.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * List of paginated records.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "results")
@XmlAccessorType(XmlAccessType.FIELD)
public class RecordResult extends AbstractDashboardResult<RecordDisplay> {

}
