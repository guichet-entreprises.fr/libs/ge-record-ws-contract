/**
 * 
 */
package fr.ge.record.ws.v1.model;

/**
 * Enumeration of all critera to sort GE/GQ record.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum RecordSortEnum {

  /** Sort by creation date. */
  CREATION("creation"),

  /** Sort by modification date. */
  UPDATE("update"),

  /** Sort by name. */
  TITLE("title");

  /** the name of the criteria. */
  private String name;

  /**
   * Constructor.
   *
   * @param typeActionName
   *          type action name
   */
  RecordSortEnum(final String name) {
    this.name = name;
  }

  /**
   * Accessor on {@link #name}.
   *
   * @return String name
   */
  public String getName() {
    return this.name;
  }

  public static String getValue(String name) {
    for (RecordSortEnum recordSortEnum : RecordSortEnum.values()) {
      if (recordSortEnum.getName().equals(name)) {
        return recordSortEnum.getName();
      }
    }
    return CREATION.getName();
  }

}
