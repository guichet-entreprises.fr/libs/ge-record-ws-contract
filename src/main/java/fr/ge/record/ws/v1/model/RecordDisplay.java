/**
 *
 */
package fr.ge.record.ws.v1.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Version of a record object for display purpose in a user interface.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
@XmlRootElement(name = "record")
@XmlAccessorType(XmlAccessType.FIELD)
public class RecordDisplay implements Serializable {

    /** UID. */
    private static final long serialVersionUID = 319842876298142980L;

    /** total number of record's steps. */
    private String totalStep;

    /** number of completed completed step. */
    private String completedStep;

    /** id of the user of records. */
    private String authorId;

    /** title. */
    private String title;

    /** error. */
    private boolean error;

    /** timestamp of last update of a record. */
    @DateTimeFormat(style = "S-")
    private Date updateDate;

    /** timestamp of creation of a record. */
    @DateTimeFormat(style = "S-")
    private Date creationDate;

    /** code. */
    private String recordId;

    /** progress. */
    private String progress;

    /** summary of the state of the record. */
    private LocalizedLabel summary;

    /** List of actions related to the record. */
    private List<RecordAction> actions;

    /** Type of the Record used only for GE Records **/
    private LocalizedLabel type;

    /** The metadata **/
    private Map<String, Collection<String>> metadata;

    /** The record details **/
    private JsonNode details;

    /**
     * Accesseur sur l'attribut {@link #actions}.
     *
     * @return List<RecordAction> actions
     */
    public List<RecordAction> getActions() {
        return this.actions;
    }

    /**
     * Mutateur sur l'attribut {@link #actions}.
     *
     * @param actions
     *            la nouvelle valeur de l'attribut actions
     */
    public void setActions(final List<RecordAction> actions) {
        this.actions = actions;
    }

    /**
     * Accesseur sur l'attribut {@link #totalStep}.
     *
     * @return String totalStep
     */
    public String getTotalStep() {
        return this.totalStep;
    }

    /**
     * Mutateur sur l'attribut {@link #totalStep}.
     *
     * @param totalStep
     *            la nouvelle valeur de l'attribut totalStep
     */
    public void setTotalStep(final String totalStep) {
        this.totalStep = totalStep;
    }

    /**
     * Accesseur sur l'attribut {@link #completedStep}.
     *
     * @return String completedStep
     */
    public String getCompletedStep() {
        return this.completedStep;
    }

    /**
     * Mutateur sur l'attribut {@link #completedStep}.
     *
     * @param completedStep
     *            la nouvelle valeur de l'attribut completedStep
     */
    public void setCompletedStep(final String completedStep) {
        this.completedStep = completedStep;
    }

    /**
     * Accesseur sur l'attribut {@link #authorId}.
     *
     * @return String authorId
     */
    public String getAuthorId() {
        return this.authorId;
    }

    /**
     * Mutateur sur l'attribut {@link #authorId}.
     *
     * @param authorId
     *            la nouvelle valeur de l'attribut authorId
     */
    public void setAuthorId(final String authorId) {
        this.authorId = authorId;
    }

    /**
     * Accesseur sur l'attribut {@link #title}.
     *
     * @return String title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Mutateur sur l'attribut {@link #title}.
     *
     * @param title
     *            la nouvelle valeur de l'attribut title
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Accesseur sur l'attribut {@link #error}.
     *
     * @return boolean error
     */
    public boolean isError() {
        return this.error;
    }

    /**
     * Mutateur sur l'attribut {@link #error}.
     *
     * @param error
     *            la nouvelle valeur de l'attribut error
     */
    public void setError(final boolean error) {
        this.error = error;
    }

    /**
     * Accesseur sur l'attribut {@link #updateDate}.
     *
     * @return long updateDate
     */
    public Date getUpdateDate() {
        return this.updateDate;
    }

    /**
     * Accesseur sur l'attribut {@link #updateDate}.
     *
     * @return String updateDate
     */
    @JsonIgnore
    public String getUpdateDateFormatted() {
        final DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
        final SimpleDateFormat sdf = (SimpleDateFormat) df;
        final String pattern = sdf.toPattern().replaceAll("y+", "yyyy");
        sdf.applyPattern(pattern);
        final String formattedDate = sdf.format(this.updateDate);
        return formattedDate;
    }

    /**
     * Mutateur sur l'attribut {@link #updateDate}.
     *
     * @param updateDate
     *            la nouvelle valeur de l'attribut updateDate
     */
    public void setUpdateDate(final Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * Accesseur sur l'attribut {@link #creationDate}.
     *
     * @return long creationDate
     */
    public Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Accesseur sur l'attribut {@link #creationDate}.
     *
     * @return String creationDate
     */
    @JsonIgnore
    public String getCreationDateFormatted() {
        final DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
        final SimpleDateFormat sdf = (SimpleDateFormat) df;
        final String pattern = sdf.toPattern().replaceAll("y+", "yyyy");
        sdf.applyPattern(pattern);
        final String formattedDate = sdf.format(this.creationDate);
        return formattedDate;
    }

    /**
     * Mutateur sur l'attribut {@link #creationDate}.
     *
     * @param creationDate
     *            la nouvelle valeur de l'attribut creationDate
     */
    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Accesseur sur l'attribut {@link #recordId}.
     *
     * @return String recordId
     */
    public String getRecordId() {
        return this.recordId;
    }

    /**
     * Mutateur sur l'attribut {@link #recordId}.
     *
     * @param recordId
     *            la nouvelle valeur de l'attribut recordId
     */
    public void setRecordId(final String recordId) {
        this.recordId = recordId;
    }

    /**
     * Accesseur sur l'attribut {@link #progress}.
     *
     * @return String progress
     */
    public String getProgress() {
        if (StringUtils.isNotBlank(this.progress)) {
            return this.progress;
        }
        final double ras = (Double.parseDouble(this.completedStep) / Double.parseDouble(this.totalStep));
        if (Double.isNaN(ras)) {
            return StringUtils.EMPTY;
        }
        final Double progress = ras * 100;
        return String.valueOf(progress.intValue());
    }

    /**
     * Mutateur sur l'attribut {@link #progress}.
     *
     * @param progress
     *            la nouvelle valeur de l'attribut progress
     */
    public void setProgress(final String progress) {
        this.progress = progress;
    }

    /**
     * Accesseur sur l'attribut {@link #type}.
     *
     * @return LocalizedLabel type
     */
    public LocalizedLabel getType() {
        return this.type;
    }

    /**
     * Mutateur sur l'attribut {@link #type}.
     *
     * @param type
     *            la nouvelle valeur de l'attribut type
     */
    public void setType(final LocalizedLabel type) {
        this.type = type;
    }

    /**
     * Accesseur sur l'attribut {@link #summary}.
     *
     * @return String summary
     */
    public LocalizedLabel getSummary() {
        return this.summary;
    }

    /**
     * Mutateur sur l'attribut {@link #summary}.
     *
     * @param summary
     *            la nouvelle valeur de l'attribut summary
     */
    public void setSummary(final LocalizedLabel summary) {
        this.summary = summary;
    }

    /**
     * Accesseur sur l'attribut {@link #serialversionuid}.
     *
     * @return long serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * Accesseur sur l'attribut {@link #metadata}.
     *
     * @return HashMap<String,String> metadata
     */
    public Map<String, Collection<String>> getMetadata() {
        return this.metadata;
    }

    /**
     * Mutateur sur l'attribut {@link #metadata}.
     *
     * @param metadata
     *            la nouvelle valeur de l'attribut metadata
     */
    public void setMetadata(final Map<String, Collection<String>> metadata) {
        this.metadata = metadata;
    }

    /**
     * Mutateur sur l'attribut {@link #metadata}.
     *
     * @param metadata
     *            la nouvelle valeur de l'attribut metadata
     */
    public void setMetadata(final String key, final String value) {
        if (null == this.metadata) {
            this.metadata = new HashMap<>();
        }

        Collection<String> values = this.metadata.get(key);
        if (null == values) {
            values = new ArrayList<>();
            this.metadata.put(key, values);
        }
        values.add(value);
    }

    /**
     * Accesseur sur l'attribut {@link #details}.
     *
     * @return JsonNode details
     */
    public JsonNode getDetails() {
        return this.details;
    }

    /**
     * Mutateur sur l'attribut {@link #details}.
     *
     * @param details
     *            la nouvelle valeur de l'attribut details
     */
    public void setDetails(final JsonNode details) {
        this.details = details;
    }

}
